package com.example.apitest.dto;

public record BlogResponseDTO(int id, String title, String message, int appuser_id) {
}