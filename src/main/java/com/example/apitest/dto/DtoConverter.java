package com.example.apitest.dto;

import com.example.apitest.entities.AppUser;
import com.example.apitest.entities.BlogPost;
import com.example.apitest.repositories.AppUserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DtoConverter {

    private final AppUserRepository appUserRepository;

    public DtoConverter(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public BlogPost RequestDtoToEntity(BlogRequestDTO blogRequestDTO){

        AppUser appUser = appUserRepository
                .findById(blogRequestDTO
                        .appUserId()).orElse(null);

        return new BlogPost(
                blogRequestDTO.title(),
                blogRequestDTO.message(),
                appUser
        );

    }

    public BlogResponseDTO entityToResponseDto(BlogPost blogPost){
        return new BlogResponseDTO(
                blogPost.getId(),
                blogPost.getTitle(),
                blogPost.getMessage(),
                blogPost.getAppUser().getId()
        );
    }

}
