package com.example.apitest.dto;

public record BlogRequestDTO(String title, String message, int appUserId) {
}