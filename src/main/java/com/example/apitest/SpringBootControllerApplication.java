package com.example.apitest;

import com.example.apitest.entities.AppUser;
import com.example.apitest.entities.BlogPost;
import com.example.apitest.repositories.AppUserRepository;
import com.example.apitest.repositories.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class SpringBootControllerApplication implements CommandLineRunner {

    @Autowired
    AppUserRepository appUserRepository;

    @Autowired
    BlogPostRepository blogPostRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootControllerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        AppUser Gunnar = new AppUser("Gunnar");
        AppUser Gunvor = new AppUser("Gunvorz");
        appUserRepository.saveAll(List.of(Gunnar, Gunvor));

        BlogPost blogPost = new BlogPost("Hej världen", "Det är sol ute", Gunnar);
        BlogPost blogPost2 = new BlogPost("Sommar!", "Köpte glass idag", Gunnar);
        BlogPost blogPost3 = new BlogPost("Hej från Gunvor", "Ville bara säg hej jag med", Gunvor);
        blogPostRepository.saveAll(List.of(blogPost, blogPost2, blogPost3));

    }
}
