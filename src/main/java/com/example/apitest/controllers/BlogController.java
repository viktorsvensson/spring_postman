package com.example.apitest.controllers;

import com.example.apitest.dto.BlogRequestDTO;
import com.example.apitest.dto.BlogResponseDTO;
import com.example.apitest.dto.DtoConverter;
import com.example.apitest.entities.BlogPost;
import com.example.apitest.services.BlogService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/blog")
public class BlogController {

    private final BlogService blogService;
    private final DtoConverter dtoConverter;

    public BlogController(BlogService blogService, DtoConverter dtoConverter) {
        this.blogService = blogService;
        this.dtoConverter = dtoConverter;
    }


    @GetMapping
    public List<BlogResponseDTO> getBlogPostList(@RequestParam(required = false) String username) {
        return blogService.findAll(username)
                .stream()
                .map(blogPost -> dtoConverter.entityToResponseDto(blogPost))
                .toList();
    }

    @GetMapping("/{id}")
    public BlogResponseDTO getBlogPostById(@PathVariable("id") int id){
        BlogPost blogPost = blogService.findBlogById(id);
        return dtoConverter.entityToResponseDto(blogPost);
    }

    @DeleteMapping("/{id}")
    public void deletePostById(@PathVariable("id") int id){
        blogService.deleteById(id);
    }

    @PostMapping
    public BlogResponseDTO createNewBlogpost(@RequestBody BlogRequestDTO blogRequestDTO){
        BlogPost blogPostIn = dtoConverter.RequestDtoToEntity(blogRequestDTO);

        BlogPost blogPostOut = blogService.createBlog(blogPostIn);
        return dtoConverter.entityToResponseDto(blogPostOut);
    }

    @PatchMapping("/{id}")
    public BlogResponseDTO patchBlogPostById(
            @PathVariable("id") int id,
            @RequestBody BlogRequestDTO changedBlogPostDTO){

        BlogPost changedBlogPost = dtoConverter.RequestDtoToEntity(changedBlogPostDTO);

        BlogPost blogPostOut = blogService.patchBlogById(id, changedBlogPost);
        return dtoConverter.entityToResponseDto(blogPostOut);
    }

    @PutMapping("/{id}")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "400", description = "missing required parameter from body", content = @Content(schema = @Schema(hidden = true)))
    public ResponseEntity<BlogResponseDTO> updateBlogPostById(
            @PathVariable("id") int id,
            @RequestBody BlogRequestDTO changedBlogPostDTO)
    {

        BlogPost changedBlogPost = dtoConverter.RequestDtoToEntity(changedBlogPostDTO);

        try {
            BlogPost blogPostOut = blogService.updateBlogById(id, changedBlogPost);
            return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            return ResponseEntity.ok(dtoConverter.entityToResponseDto(null));
        }
        
    }

}
