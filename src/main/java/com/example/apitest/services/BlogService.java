package com.example.apitest.services;

import com.example.apitest.entities.BlogPost;
import com.example.apitest.repositories.BlogPostRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogService {

    BlogPostRepository blogPostRepository;

    public BlogService(BlogPostRepository blogPostRepository){
        this.blogPostRepository = blogPostRepository;
    }

    public List<BlogPost> findAll(String username) {
        if(username == null)
            return blogPostRepository.findAll();

        return blogPostRepository.findByAppUser_UsernameStartingWith(username);
    }

    public BlogPost findBlogById(int id) {
        return blogPostRepository.findById(id).orElseThrow();
    }

    public void deleteById(int id) {
        blogPostRepository.deleteById(id);
    }

    public BlogPost createBlog(BlogPost blogPost) {
        return blogPostRepository.save(blogPost);
    }

    public BlogPost patchBlogById(int id, BlogPost changedBlogPost) {

        BlogPost blogPost = blogPostRepository.findById(id).orElseThrow();

        if(changedBlogPost.getTitle() != null)
            blogPost.setTitle(changedBlogPost.getTitle());
        if(changedBlogPost.getMessage() != null)
            blogPost.setMessage(changedBlogPost.getMessage());
        if(changedBlogPost.getAppUser() != null)
            blogPost.setAppUser(changedBlogPost.getAppUser());

        return blogPostRepository.save(blogPost);
    }

    public BlogPost updateBlogById(int id, BlogPost changedBlogPost) {

        BlogPost blogPost = blogPostRepository.findById(id).orElseThrow();

        BeanUtils.copyProperties(changedBlogPost, blogPost, "id");

        return blogPostRepository.save(blogPost);
    }
}
